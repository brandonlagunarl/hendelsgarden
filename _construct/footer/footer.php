<div>
<div id="mapa"></div>
</div>
<footer>
      <div class="row" id="piecera">
        <div class="col l4" id="pntvent">
          <p id="title_pt">Agentes de Venta </p>
          <!-- Aqui van las cartasde información -->
          <div id="agents">
            
          </div>
          <!-- Aqui termina las cartas de informacion -->
        </div>
        <div class="col l4" id="centrofooter">
            <div id="formulario">
                <h6>Escriba</h6>
                <input type="text" placeholder="Nombre">
                <input type="email" placeholder="Correo">
                <input type="text" placeholder="Telefono">
                <input type="text" placeholder="Ciudad">
                <textarea name="" id="" cols="30" rows="10" placeholder="Escriba aquí su mensaje"></textarea>
                <a  class="btnBuy waves-effect waves-light waves-blue" id="send" href="#!">Enviar</a>                
              </div>
              <div id="redes">
                <p>Siganos en:</p>
                <div id="socialicon">
                  <a href=""><i class="fab fa-facebook-f"></i></a>
                  <a href=""><i class="fab fa-twitter"></i></a>
                </div>
              </div>
        </div>
        <div class="col l4">
            <p id="title_geo"> de Agentes</p>
          <div id="map">

          </div>
        </div>
        <div class="col l12" id="copyright">
          <div class="col s12 l6"><i class="fas fa-copyright "></i> 2018 TICRB</div>
          <div class="col s12 l6 right-align" id="terminos">
            <a href="">Terminos y condiciones </a><i>|</i>
            <a href="">Avisos de privacidad</a><i> | </i>
            <a href="">Protección de datos personales</a><i> | </i>
            <a href="">Uso de cookies</a>
          </div>
        </div>
      </div>
    </footer>