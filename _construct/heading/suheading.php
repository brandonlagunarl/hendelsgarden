<nav>
    <div class="nav-wrapper">
      <a href="#!" class="brand-logo brand-adm-logo"><i class="material-icons">cloud</i>Dashboard Hendel</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="#search" id="search" launch="brand-logo"><i class="material-icons" >search</i></a></li>
        <li><a href="#modules" launch="response"><i class="material-icons">view_module</i></a></li>
        <li><a href="#reload" launch=""><i class="material-icons">refresh</i></a></li>
        <li><a href="#list" launch=""><i class="material-icons">more_vert</i></a></li>
        <li><a href="#logout" launch="rtrn" ><i class="material-icons">power_settings_new</i></a></li>
      </ul>
    </div>
  </nav>