$(document).ready(function(){
    $('.modal').modal();
    $('.collapsible').collapsible();
    $('.tooltipped').tooltip();
    $('select').formSelect();
    $('.collapsible').collapsible();
    $('.datepicker').datepicker();
    $('.timepicker').timepicker();
});

  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.fixed-action-btn');
    var instances = M.FloatingActionButton.init(elems, {
      direction: 'left',
      hoverEnabled: false
    });
  });

  $('.button').on('click', function(event){
    var type = $(this).data('type');
    var status = $(this).data('status');
    
    $('.button').removeClass('is-active');
    $(this).addClass('is-active');
    
    $('.notify')
      .removeClass()
      .attr('data-notification-status', status)
      .addClass(type + ' notify')
      .addClass('do-show'); 
    
    event.preventDefault();
  })
  /* hover card */
$(".hover-card").mouseleave(
  function () {
    $(this).removeClass("hover-card");
  }
);

  
