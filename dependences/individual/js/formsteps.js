var form = $("#contact");
form.validate({
    errorPlacement: function errorPlacement(error, element) { element.before(error); },
    rules: {
        confirm: {
            equalTo: "#password"
        }
    }
});
form.children("div").steps({
    headerTag: "h5",
    bodyTag: "section",
    transitionEffect: "slideLeft",
    onStepChanging: function (event, currentIndex, newIndex)
    {
        form.validate().settings.ignore = ":disabled,:hidden";
        return form.valid();
    },
    onFinishing: function (event, currentIndex)
    {
        form.validate().settings.ignore = ":disabled";
        return form.valid();
    },
    onFinished: function (event, currentIndex)
    {
        $('#finish').click(function(){
             var name =$("#buyername").val();
             var lastname =$("#buyerlastname").val();
             var document =$("#document").val();
             var city =$("#city").val();
             var email =$("#email").val();
             var phone =$("#phone").val();
             var delivery =$("#delivery").val();
             var anexed =$("#anexed").val();
             var datepicker =$("#datepicker").val();
             var payment =$('input:radio[name=payment-1]:checked').val();
           
            if($.trim(name).length > 0 && $.trim(document).length > 0){
                $.ajax({
                    url:"index.php?controller=Checkout",
                    method: "POST",
                    data: {name:name,lastname:lastname,document:document,city:city,email:email,phone:phone,delivery:delivery,anexed:anexed,datepicker:datepicker,payment:payment},
                    cache : "false",
                    beforeSend:function(){
                        $('#everest').html( "<div class='progress fx'><div class='indeterminate'></div></div>" );
                        M.toast({html: name+' tu factura esta siendo registrada'})
                        $("#bfinish").css( "display", "none");
                        $('#bprevious').addClass("disabled");
                        $("#bprevious").css( "display", "none");
                    },
                    success:function(data){
                        $('#everest').html( "<div class='progress fx'><div class='indeterminate'></div></div>" );
                        $('#everest').html("<div id='everest'></div>");
                        $('#finish').val("Finish");
                        M.toast({html: 'factura realizada '+name})
                        $('#bfinish').addClass("disabled");
                        $("#bfinish").css( "display", "none");
                        $('#bprevious').addClass("disabled");
                        $("#bprevious").css( "display", "none");
                        $(location).attr('href', 'Checkout?action=validation&i=1'); 
                        //$(location).attr('href','Admin'); 
                        
                    }
                });
            };
        });
    }
});

