$(document).ready(function() {
  action();
});
function action() {
  $("a").click(function() {
    var exp = $(this).attr("href");
    if (exp.includes("#") == true) {
      exp = exp.replace("#", "");
    }
    var launch = $(this).attr("launch");
    if (launch.includes("rtrn") == true) {
      sessionStorage.clear();
      window.location.href = "Index";
    } else {
    }
    var func = { func: exp, launch: launch };
    $.post(
      "Admin?action=" + exp,
      {
        exp: exp,
        launch: launch
      },
      function(response, status) {
        $("." + launch).html(response);
      }
    );
  });
}
