<div class="container-fluid" style=";">
<div class="row contenedor info_service center-align">
    <h4>Servicios</h4>
    <div class="col l12 left-align">
        <!-- Aqui va los servicios -->
        <!-- //////////////// -->
        <div class="col m3 l3 section_info">
            <img src="media/icon/svg/002-buy.svg" alt="">
            <h6>Contraentrega</h6>
            <p>Una pequeña descripcion de esta parte de la sección para que el cliente se entusiasme más en comprarnos</p>
        </div>
        <!-- //////////////// -->
        <div class="col m3 l3 section_info">
            <img src="media/icon/svg/001-shipped.svg" alt="">
            <h6>Envio seguro</h6>
            <p>Una pequeña descripcion de esta parte de la sección para que el cliente se entusiasme más en comprarnos</p>
        </div>
        <!-- //////////////// -->
        <div class="col m3 l3 section_info">
            <img src="media/icon/svg/003-online-shop.svg" alt="">
            <h6>Pagos en linea</h6>
            <p>Una pequeña descripcion de esta parte de la sección para que el cliente se entusiasme más en comprarnos</p>
        </div>
        <!-- //////////////// -->
        <div class="col m3 l3 section_info">
            <img src="media/icon/svg/004-woman-with-headset.svg" alt="">
            <h6>Asesorias</h6>
            <p>Una pequeña descripcion de esta parte de la sección para que el cliente se entusiasme más en comprarnos</p>
        </div>
        <!-- //////////////// -->
        <div class="col m3 l3 section_info">
            <img src="media/icon/svg/005-24-hours-delivery.svg" alt="">
            <h6>Entrega inmediata</h6>
            <p>Una pequeña descripcion de esta parte de la sección para que el cliente se entusiasme más en comprarnos</p>
        </div>
        <!-- //////////////// -->
        <div class="col m3 l3 section_info">
            <img src="media/icon/svg/006-colombia.svg" alt="">
            <h6>Entregas nacionales</h6>
            <p>Una pequeña descripcion de esta parte de la sección para que el cliente se entusiasme más en comprarnos</p>
        </div>
        <!-- //////////////// -->
        <div class="col m3 l3 section_info">
            <img src="media/icon/svg/007-guarantee.svg" alt="">
            <h6>compra segura</h6>
            <p>Una pequeña descripcion de esta parte de la sección para que el cliente se entusiasme más en comprarnos</p>
        </div>
        <!-- //////////////// -->
        <div class="col m3 l3 section_info">
            <img src="media/icon/svg/008-money-back.svg" alt="">
            <h6>Devolución</h6>
            <p>Una pequeña descripcion de esta parte de la sección para que el cliente se entusiasme más en comprarnos</p>
        </div>
    </div>
</div>
</div>