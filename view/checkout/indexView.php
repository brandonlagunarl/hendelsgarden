<title>HendelsGarden <?=$tag?></title>
<link rel="stylesheet" href="dependences/individual/css/formsteps.css">
<div class="container-fluid" style="position:relative; top:100px;">
    <div class="row">
        <div class="col s12 m3 l3">
            <?php
            $this->frameview("cart/miniature",array(
                "getcart" => $getcart,
                "getall" => $getall,
                "paymentmethod" => $paymentmethod,
            ));
            ?>
            <p id="rescity"></p>
            <p id="resultadod"></p>
        </div>

        <div class="col s12 m9 l9">
<form id="contact" method="POST">
    <div>
        <h5></h3>
    <section>
        <legend>Ten en cuenta los datos requeridos (*)</legend>
        <label for="buyername">Nombres *</label>
        <input id="buyername" name="buyername" type="text" class="required">
        <label for="buyerlastname">Apellidos *</label>
        <input id="buyerlastname" name="buyerlastname" type="text" class="required">
        <label for="document">Numero de Documento (+18) *</label>
        <input id="document" name="document" type="text" class="required">
        <p>(*) Requeridos</p>
    </section>
        <h5></h5>
    <section>
    <legend>Informacion importante</legend>
        <label for="city">Ciudad *</label>
        <input type="text" id="city" name="city" class="autocomplete required">
        <label for="delivery">Direccion *</label>
        <input id="delivery" name="delivery" type="text" class="required">
        <label for="email">Email *</label>
        <input id="email" name="email" type="text" class="required email">
        <p>(*) Requeridos</p>
    </section>
        <h5></h5>
    <section>
        <legend>Datos de seguimiento</legend>
        <label for="phone">Telefono *</label>
        <input id="phone" name="phone" type="text" class="required">
        <label for="anexed">Anexa algun dato de tu residencia ej:(apto 101 / puerta roja) *</label>
        <textarea name="anexed" id="anexed" cols="40" rows="20"></textarea>
        <label for="datepicker">Deseas tu entrega en un dia especifico? </label>
        <input type="text" class="datepicker" id="datepicker" name="datepicker">
    </section>
    <h5></h5>
    <section>
    <legend>Elige un medio de pago (<a href="">puedes ver informacion importante de tu interes aqui<a/>)</legend>
            <?php foreach ($paymentmethod as $method) { ?>
                <p>
                    <label>
                        <input class="with-gap" id="<?=$method->name?>" name="<?=$method->name?>" value="<?=$method->value?>" type="radio" required />
                        <span><?=$method->PTitle?></span>
                    </label>
                </p>
            <?php } ?>

    </section>
    </div>
</form>
<span id="tst" class="tst"></span>
</div>
</div>
</div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('.datepicker').datepicker();
  });
</script>
<script src='https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js'></script>
    <script  src="dependences/individual/js/formsteps.js"></script>
