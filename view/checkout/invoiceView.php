<link rel="stylesheet" href="dependences/individual/css/invoice.css">
<?php foreach ($validation as $check) {} ?>
<div id="invoiceholder">
<div id="headerimage"></div>
<div id="invoice" class="effect2">
  
  <div id="invoice-top">
    <div class="logo"></div>
    <div class="info">
      <h2>Hendels Colombia</h2>
      <p>info@hendelsgarden.com</br>
          3002743457
      </p>
    </div><!--End Info-->
    <div class="title">
      <p>Emitido: <?=$check->DateEntered?></br>
         Fecha de Pago: 
      </p>
    </div><!--End Title-->
  </div><!--End InvoiceTop-->

  <div id="invoice-mid">
    
    <div class="clientlogo"></div>
    <div class="info">
      <h2><?=$check->BuyerName." ".$check->BuyerLastName?></h2>
      <p><?=$check->BuyerEmail?></br>
      <?=$check->Document?></br>
    </div>

    <div id="project">
      <h2>Descripcion de la compra</h2>
      <p>Productos Hendels.</p>
    </div>   

  </div><!--End Invoice Mid-->
  
  <div id="invoice-bot">
    
    <div id="table">
      <table>
        <tr class="tabletitle">
          <td class="">Producto</td>
          <td class="">Unidades</td>
          <td class="">Codigo</td>
          <td class="">Valor U.</td>
          <td class="">Sub-total</td>
        </tr>
        <?php foreach($getall as $product){ ?>

        <tr class="service">
          <td class="tableitem"><p class="itemtext"><?=$product->NameProduct?></p></td>
          <td class="tableitem"><p class="itemtext"><?=$product->Quantity?></p></td>
          <td class="tableitem"><p class="itemtext"></p></td>
          <td class="tableitem"><p class="itemtext"><?=$product->PriceProduct?></p></td>
          <td class="tableitem"><p class="itemtext"><?= number_format($product->PriceProduct * $product->Quantity, 3, '.', ',')?></p></td>
        </tr>
        <?php }?>
        
          <?php 
          $sumado = 0;
          foreach ($getall as $product){
          $sumado += number_format($product->PriceProduct * $product->Quantity, 3, '.', ',');  
          }
            ?>
        <tr class="tabletitle">
          <td></td>
          <td></td>
          <td class="Rate"><h2>Total</h2></td>
          <td class="payment"><h2>$<?=number_format($sumado, 3, '.', ',')?></h2></td>
        </tr>
        
      </table>
    </div><!--End Table-->
    
  <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
    <input type="hidden" name="cmd" value="_s-xclick">
    <input type="hidden" name="hosted_button_id" value="QRZ7QTM9XRPJ6">
    <input type="image" src="http://michaeltruong.ca/images/paypal.png" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
  </form>

    
    <div id="legalcopy">
      <p class="legal"><strong>Thank you for your business!</strong>  Payment is expected within 31 days; please process this invoice within that time. There will be a 5% interest charge per month on late invoices. 
      </p>
    </div>
    
  </div><!--End InvoiceBot-->
</div><!--End Invoice-->
</div><!-- End Invoice Holder-->


