<link rel="stylesheet" href="dependences/individual/css/subpages.css">
<title>HendelsGarden | <?=$tag?></title>
<div style="height:; width:100%;"></div>
<header>
  <div class="header-title">
    Paginas sugeridas
  </div>
  <div class="bars">
    <ul>
      <li class="cor-1"></li>
      <li class="cor-2"></li>
      <li class="cor-3"></li>
      <li class="cor-4"></li>
      <li class="cor-5"></li>
    </ul>
  </div>
</header>
<?php foreach($productpage as $product){
?>
<section class="content-block wow slideInRight">
  <h1><?=$product->NameProduct?></h1>
  <h2><?=$product->DescProduct?></h2>
  <p><?=$product->LongDescProduct?></p>
  <a href="Products?action=Detail&i=<?=$product->MetaKeyWord?>" class="waves-effect waves-teal btn-flat pulse">Descripcion del producto</a>
  <a href="<?=$product->ReLink?>" class="btn waves-effect waves-light pulse">Visitar Pagina OFICIAL</a>
  
</section>

<?php
}
?>