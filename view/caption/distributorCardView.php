
<div class="container-fluid centered" style="padding:2em 10px;">
<div class="row">
<?php foreach ($agents as $agent) { ?>
   <div class="col s6 m3">
   <div class="card">
    <div class="card-image waves-effect waves-block waves-light">
      <img class="activator" src="media/img/office.jpg">
    </div>
    <div class="card-content">
      <span class="card-title activator grey-text text-darken-4"><?=$agent->AgentName?><i class="material-icons right">more_vert</i></span>
      <p><a href="#"><?=$agent->municipio?> / <?=$agent->departamento?></a></p>
    </div>
    <div class="card-reveal">
      <span class="card-title grey-text text-darken-4"><?=$agent->AgentEmail?><i class="material-icons right">close</i></span>
      <p>biografia</p>
    </div>
  </div>
   </div>
<?php } ?>
</div>
</div>