<title><?=$tag?></title>
<link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
<link rel="stylesheet" href="dependences/individual/css/formsteps.css">

<div class="container">
<form id="contact" action="Question?action=trabaja_con_nosotros" method="POST">
    <div>
        <h3>Datos Personales</h3>
        <section>
        <input type="hidden" name="catch" id="catch" value="workwithus">
        <label for="name">First name *</label>
            <input id="name" name="first_name" type="text" class="required">
            <label for="surname">Last name *</label>
            <input id="surname" name="last_name" type="text" class="required">
            <label for="email">Email *</label>
            <input id="email" name="email" type="text" class="required email">
            <p>(*) Obligatorio</p>
        </section>
        <h3>Tu Ubicacion</h3>
        <section>
        <label for="address">Address</label>
            <input id="address" name="address" type="text">
            <label for="city">City</label>
            <input id="city" name="city" type="text">
            <label for="phone">Phone</label>
            <input id="phone" name="phone" type="text">
        </section>
        <h3>Hints</h3>
        <section>
            <ul>
                <li>Foo</li>
                <li>Bar</li>
                <li>Foobar</li>
            </ul>
        </section>
        <h3>Finish</h3>
        <section>
        <p>
      <label>
        <input id="indeterminate-checkbox" type="checkbox" id="acceptTerms" name="acceptTerms" class="required"/>
        <span>Acepto los terminos</span>
      </label>
    </p>
        </section>
    </div>
    <button type="submit">Enviar</button>
</form>
</div>
<script type="text/javascript">
  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.autocomplete');
    var instances = M.Autocomplete.init(elems, options);
  });


  // Or with jQuery

  $(document).ready(function(){
    $('input.autocomplete').autocomplete({
      data: {
        "Apple": null,
        "Microsoft": null,
        "Google": 'https://placehold.it/250x250'
      },
    });
  });
</script>
<script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
<script src='https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.js'></script>
    <script  src="dependences/individual/js/formsteps.js"></script>
