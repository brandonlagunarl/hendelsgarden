<table>
<thead style="font-size:10pt;">
     <tr>
        <th>Vista</th>
        <th>Producto</th>
        <th>V. Unitario</th>
        <th>V. Total</th>
     </tr>
  </thead>
<tbody>
<?php foreach($getall as $product){ ?>
<tr>
  <td><img src="media/img/<?=$product->ImgProduct?>" alt="" class="circle" style="width:30px;"></td>
   <td><?=$product->NameProduct?> (<?=$product->Quantity?>)</td>
   <td>$<?=$product->PriceProduct?></td>
   <td>$<?= number_format($product->PriceProduct * $product->Quantity, 3, '.', ',')?></td>
</tr>
<?php } ?>
</tbody>
</table>