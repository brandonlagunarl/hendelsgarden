<title>HendelsGarden | <?=$tag?></title>
<style>
  .plus{margin-left:9px;}
  .less{ }
</style>
<div style="width:100%; height:100px;"></div>
<div class="container-fluid">
<?php foreach($getall as $product){ ?>
<div class="item" id="secproduct">
  <div class="item--image">
    <img src="media/img/<?=$product->ImgProduct?>">
  </div>
  <div class="item--description">
    <h3><?=$product->NameProduct?></h3>
    <p><?=$product->DescProduct?></p>
    <a class="btn-floating waves-effect waves-light btn-small trash" id="<?=$product->id?>" onclick="deleteitem(<?=$product->id?>)"><i class="material-icons">delete</i></a>
  </div>
  <div class="item--stepper">
    <div class="row">
      <div class="col s4"><a class="btn-floating btn-small waves-effect waves-light less ki4ra" id="less_<?=$product->id?>"><i class="material-icons">remove</i></a></div>
      <div class="col s4 "><a class="btn-flat disabled cnt" id="C4n71d4d_<?=$product->id?>"><?=$product->Quantity?></a></div>
      <div class="col s4"><a class="btn-floating btn-small waves-effect waves-light plus ki4ra" id="plus_<?=$product->id?>"><i class="material-icons">add</i></a></div>
    </div>
    
  </div>
  <div class="item--price">
  <?= number_format($product->PriceProduct * $product->Quantity, 3, '.', ',')?>
  </div>
</div>
<?php } ?>
</div>

<div class="container">
<a class="waves-effect waves-light btn" href="Checkout" id="departament">Finalizar Compra</a>
</div>