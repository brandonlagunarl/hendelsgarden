<?php
class Check extends EntidadBase{

    private $id_check;
    private $BuyerName;
    private $BuyerLastName;
    private $Document;
    private $BuyerEmail;
    private $BuyerPhone;
    private $BuyerCity;
    private $BuyerDelivery;
    private $BuyerAnexedDelivery;
    private $DeliveryTime;
    private $Token;
    private $Ip;
    private $DateEntered;
    private $Delivered;
    private $Actived;
    private $Payment;
    private $transactionState;

    public function __construct($adapter) {
        $table ="tb_sales_check";
        parent:: __construct($table, $adapter);
    }
    public function getId_check()
    {
        return $this->id_check;
    } 
    public function setId_check($id_check)
    {
        $this->id_check = $id_check;
    } 
    public function getBuyerName()
    {
        return $this->BuyerName;
    } 
    public function setBuyerName($BuyerName)
    {
        $this->BuyerName = $BuyerName;
    } 
    public function getBuyerLastName()
    {
        return $this->BuyerLastName;
    } 
    public function setBuyerLastName($BuyerLastName)
    {
        $this->BuyerLastName = $BuyerLastName;
    } 
    public function getDocument()
    {
        return $this->Document;
    } 
    public function setDocument($Document)
    {
        $this->Document = $Document;
    } 
    public function getBuyerEmail()
    {
        return $this->BuyerEmail;
    } 
    public function setBuyerEmail($BuyerEmail)
    {
        $this->BuyerEmail = $BuyerEmail;
    } 
    public function getBuyerPhone()
    {
        return $this->BuyerPhone;
    } 
    public function setBuyerPhone($BuyerPhone)
    {
        $this->BuyerPhone = $BuyerPhone;
    }  
    public function getBuyerCity()
    {
        return $this->BuyerCity;
    } 
    public function setBuyerCity($BuyerCity)
    {
        $this->BuyerCity = $BuyerCity;
    }
    public function getBuyerDelivery()
    {
        return $this->BuyerDelivery;
    } 
    public function setBuyerDelivery($BuyerDelivery)
    {
        $this->BuyerDelivery = $BuyerDelivery;
    } 
    public function getBuyerAnexedDelivery()
    {
        return $this->BuyerAnexedDelivery;
    } 
    public function setBuyerAnexedDelivery($BuyerAnexedDelivery)
    {
        $this->BuyerAnexedDelivery = $BuyerAnexedDelivery;
    }  
    public function getDeliveryTime()
    {
        return $this->DeliveryTime;
    } 
    public function setDeliveryTime($DeliveryTime)
    {
        $this->DeliveryTime = $DeliveryTime;
    } 
    public function getToken()
    {
        return $this->Token;
    } 
    public function setToken($Token)
    {
        $this->Token = $Token;
    } 
    public function getIp()
    {
        return $this->Ip;
    } 
    public function setIp($Ip)
    {
        $this->Ip = $Ip;
    }
    public function getDateEntered()
    {
        return $this->DateEntered;
    } 
    public function setDateEntered($DateEntered)
    {
        $this->DateEntered = $DateEntered;
    } 
    public function getDelivered()
    {
        return $this->Delivered;
    } 
    public function setDelivered($Delivered)
    {
        $this->Delivered = $Delivered;
    }
    public function getActived()
    {
        return $this->Actived;
    }
    public function setActived($Actived)
    {
        $this->Actived = $Actived;
    }
    public function getPayment()
    {
        return $this->Payment;
    }
    public function setPayment($Payment)
    {
        $this->Payment = $Payment;
    }
    public function getTransactionState()
    {
        return $this->transactionState;
    }
    public function setTransactionState($transactionState)
    {
        $this->transactionState = $transactionState;
    }

    public function savedata()
    {
        
        $filter= "SELECT * FROM `tb_sales_check` WHERE Token = '".$this->Token."'";
        $resultfilter = $this->db()->query($filter);
        if ($resultfilter->num_rows  == 0) {
        $query = "INSERT INTO `tb_sales_check` (`id_check`,`BuyerName`,`BuyerLastName`,`Document`,`BuyerCity`,`BuyerEmail`,`BuyerPhone`,`BuyerDelivery`,`BuyerAnexedDelivery`,`Payment`,`DeliveryTime`,`Token`,`Ip`,`Actived`)
        VALUES(NULL,
            '".$this->BuyerName."',
            '".$this->BuyerLastName."',
            '".$this->Document."',
            '".$this->BuyerCity."',
            '".$this->BuyerEmail."',
            '".$this->BuyerPhone."',
            '".$this->BuyerDelivery."',
            '".$this->BuyerAnexedDelivery."',
            '".$this->Payment."',
            '".$this->DeliveryTime."',
            '".$this->Token."',
            '".$this->Ip."',
            '".$this->Actived."'
            )";
            
            $savedata=$this->db()->query($query);
                if($savedata){
                    $data ="ok";
                    return json_encode($data);
                }
                return $savedata;
            }

    }
    public function getCheck($notouch,$token)
    {
        $query = $this->db()->query("SELECT * FROM tb_sales_check WHERE ip = '$notouch' AND Token = '$token' AND Actived = 0");

        if($query->num_rows > 0){
           
            while ($row = $query->fetch_object()) {
               $resultSet[]=$row;
            }
         
         return $resultSet;
        }
        else{
            header("location: Index");
        }
    }

    public function getCheckAlert($notouch,$token)
    {
        $query = $this->db()->query("SELECT * FROM tb_sales_check WHERE ip = '$notouch' AND Token = '$token' AND Actived = 1 ");

        if($query->num_rows > 0){
           
            while ($row = $query->fetch_object()) {
               $resultSet[]=$row;
            }
         
         return $resultSet;
        }
        else{
            header("location: Index");
        }
    }

    public function savestatus($id)
    {
        $filter= "SELECT * FROM `tb_sales_check` WHERE id_check = '$id' AND Token = '".$this->Token."'";
        $resultfilter = $this->db()->query($filter);
        if ($resultfilter->num_rows  == 1) {
            $query ="UPDATE `tb_sales_check` SET
            `Actived`           =       '".$this->Actived."', 
            `transactionState`  =      '".$this->transactionState."'
            WHERE id_check = '$id' AND Token = '".$this->Token."';";
        $updatedate =$this->db()->query($query);
        return $updatedate;

        }
    }

    public function delivery($data)
    {
        $query = $this->db()->query("SELECT * FROM  WHERE ");

    }

    
    
}

?>
