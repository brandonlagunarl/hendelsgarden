<?php
class Discount extends EntidadBase{

    private $id, $idProduct, $StartDate, $EndDate, $DiscountPercent, $Active;

    public function __construct($adapter) {
        $table ="discount";
        parent:: __construct($table,$adapter);
    }

    public function GetId()
    { 
        return $this->id; 
    }
    public function SetId($id)
    { 
        $this->id =$id; 
    }

    public function GetidProduct()
    { 
        return $this->idProduct; 
    }
    public function Set($idProduct)
    { 
        $this->idProduct =$idProduct; 
    }
    public function GetStartDate()
    { 
        return $this->StartDate; 
    }
    public function SetStartDate($StartDate)
    { 
        $this->StartDate =$StartDate; 
    }
    public function GetEndDate()
    { 
        return $this->EndDate; 
    }
    public function SetEndDate($EndDate)
    { 
        $this->EndDate =$EndDate; 
    }
    public function GetDiscountPercent()
    { 
        return $this->DiscountPercent; 
    }
    public function SetDiscountPercent($DiscountPercent)
    { 
        $this->DiscountPercent =$DiscountPercent; 
    }
    public function GetActive()
    { 
        return $this->Active; 
    }
    public function SetActive($Active)
    { 
        $this->Active =$Active; 
    }

    public function restDays()
    {
        
            $dia_mundial=mktime(0, 0, 0, 10, 12, 2018); 
            $hoy=time(); 

            $faltante_segundos=$dia_mundial-$hoy; 

            $faltante_dias=ceil(abs($faltante_segundos/86400)); 

            return $faltante_dias;    
    }
}

?>