<?php
class FormModel extends ModeloBase{
    private $table;

    public function __construct($adapter) {
        $this->table ="scheduled_appointments";
        parent:: __construct($this->table,$adapter);
    }

    //Form list by actives upcoming events
    public function upcomingevents(){
        //get actual datetime on MYSQL Format
        $actualdate = date("Y-m-d H:i:s"); 
        $query=$this->db()->query("SELECT * FROM scheduled_appointments WHERE `Activate` = 1 AND `Date` > $actualdate"); 
        while ($row = $query->fetch_object()) {
            $resultSet[]=$row;
         }
         return $resultSet;
    }

    
}

?>