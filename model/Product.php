<?php
class Product extends EntidadBase{
    private $id;
    private $NameProduct;
    private $PriceProduct;
    private $DescProduct;
    private $ActiveDiscount;
    private $DiscountPercent;
    private $ActiveProduct;
    private $idFamily;
    private $DateEntered;
    private $ReLink;

    public function __construct($adapter) {
        $table ="product";
        parent:: __construct($table,$adapter);
    }

    public function GetId()
    { 
        return $this->id; 
    }
    public function SetId($id)
    { 
        $this->id =$id; 
    }
    public function GetNameProduct()
    { 
        return $this->NameProduct; 
    }
    public function SetNameProduct($NameProduct)
    {
        $this->NameProduct = $NameProduct;
    }
    public function getPriceProduct()
    {
        return $this->PriceProduct;
    }
    public function setPriceProduct($PriceProduct)
    {
        $this->PriceProduct = $PriceProduct;
    }
    public function getDescProduct()
    {
        return $this->DescProduct;
    }
    public function setDescProduct($DescProduct)
    {
        $this->DescProduct = $DescProduct;
    }
    public function getMetaKeyWord()
    {
        return $this->MetaKeyWord;
    }
    public function setMetaKeyWord($MetaKeyWord)
    {
        $this->MetaKeyWord = $MetaKeyWord;
    }
    public function getActiveDiscount()
    {
        return $this->ActiveDiscount;
    }
    public function setActiveDiscount($ActiveDiscount)
    {
        $this->ActiveDiscount = $ActiveDiscount;
    }
    public function getDiscountPercent()
    {
        return $this->DiscountPercent;
    }
    public function setDiscountPercent($DiscountPercent)
    {
        $this->DiscountPercent = $DiscountPercent;
    }
    public function getActiveProduct()
    {
        return $this->ActiveProduct;
    }
    public function setActiveProduct($ActiveProduct)
    {
        $this->ActiveProduct = $ActiveProduct;
    }
    public function getidFamily()
    {
        return $this->idFamily;
    }
    public function setidFamily($idFamily)
    {
        $this->idFamily = $idFamily;
    }
    public function getDateEntered()
    {
        return $this->DateEntered;
    }
    public function setDateEntered($DateEntered)
    {
        $this->DateEntered = $DateEntered;
    }
    public function GetReLink()
    { 
        return $this->ReLink; 
    }
    public function SetReLink($ReLink)
    { 
        $this->ReLink =$ReLink; 
    }

    public function getProductAll()
    {
        $query=$this->db()->query("SELECT * FROM product INNER JOIN discount ON product.id = discount.idProduct");
        
        if($query->num_rows > 0){
           
        while ($row = $query->fetch_object()) {
           $resultSet[]=$row;
           $rows = $query->num_rows;
        }
    }
    else{
        echo "0 Rows";
    }
        return $resultSet;
    }

    public function getProductByName($name)
    {
        $query=$this->db()->query("SELECT * FROM product INNER JOIN discount ON product.id = discount.idProduct WHERE MetaKeyWord = '$name'");
        
        if($query ->num_rows > 0){
        while ($row = $query->fetch_object()) {
            
           $resultSet[]=$row;
        }
    }
    else{
        
    }
        return $resultSet;
    }

    public function getProductsbyCategory($category)
    {
        $query=$this->db()->query("SELECT * FROM product INNER JOIN discount ON product.id = discount.idProduct WHERE idFamily= '$category'");
        
        if($query ->num_rows > 0){
        while ($row = $query->fetch_object()) {
            
           $resultSet[]=$row;
        }

    }
    else{
        $resultSet = "0 Rows";
        return $resultSet;
    }
        return $resultSet;
    }

    public function saveproduct(){
        $query ="INSERT INTO `product` (`id`,`NameProduct`,`PriceProduct`,`DescProduct`,`ReLink`,`idFamily`,`ActiveProduct`,`ActiveDiscount`,`MetaKeyWord`)
        VALUES (NULL,
                '".$this->NameProduct."',
                '".$this->PriceProduct."',
                '".$this->DescProduct."',
                '".$this->ReLink."',
                '".$this->idFamily."',
                '".$this->ActiveProduct."',
                '".$this->ActiveDiscount."',
                '".$this->MetaKeyWord."')";
            
       $saveproduct=$this->db()->query($query);
       if($saveproduct){
           $status ="Product Was Successfully Created.";
           return json_encode($status);
       }else{
        $status ="Error.";
        return json_encode($status);
       }
       return $saveproduct;
    }

    public function GetItemsByCart($idCart,$Token)
    {
        $query = $this->db()->query("SELECT * FROM tb_item_cart INNER JOIN product ON tb_item_cart.idProduct = product.id AND tb_item_cart.Token = '$Token' WHERE idCart = '$idCart' OR Token = '$Token'");
        if($query->num_rows > 0){
            while ($row = $query->fetch_object()) {
               $resultSet[]=$row;
                }
                return $resultSet;
                }
                else{
                     header("location: Index");
                    }
    }

    

}
?>