<?php
class Chat extends EntidadBase{

    private $id_room;
    private $id_recept;
    private $id_transmiter;
    private $ip_transmiter;
    private $Token;
    // for message
    private $id_message;
    private $id_message_transmiter;
    private $ip;
    private $TokenMessage;
    private $Message;

    public function __construct($adapter) {
        $table ="chat_room";
        parent:: __construct($table, $adapter);
    }
    public function getid_room()
    {
        return $this->id_room;
    }
    public function Setid_room($id_room)
    {
        $this->id_room = $id_room;
    }

    public function getid_recept()
    {
        return $this->id_recept;
    }
    public function Setid_recept($id_recept)
    {
        $this->id_recept = $id_recept;
    }
    public function getid_transmiter()
    {
        return $this->id_transmiter;
    }
    public function Setid_transmiter($id_transmiter)
    {
        $this->id_transmiter = $id_transmiter;
    }
    public function getip_transmiter()
    {
        return $this->ip_transmiter;
    }
    public function Setip_transmiter($ip_transmiter)
    {
        $this->ip_transmiter = $ip_transmiter;
    }
    public function getToken()
    {
        return $this->Token;
    }
    public function SetToken($Token)
    {
        $this->Token = $Token;
    }
    public function getid_message()
    {
        return $this->id_message;
    }
    public function Setid_message($id_message)
    {
        $this->id_message = $id_message;
    }
    public function getid_message_transmiter()
    {
        return $this->id_message_transmiter;
    }
    public function Setid_message_transmiter($id_message_transmiter)
    {
        $this->id_message_transmiter = $id_message_transmiter;
    }
    public function getip()
    {
        return $this->ip;
    }
    public function Setip($ip)
    {
        $this->ip = $ip;
    }
    public function getTokenMessage()
    {
        return $this->TokenMessage;
    }
    public function SetTokenMessage($TokenMessage)
    {
        $this->TokenMessage = $TokenMessage;
    }
    public function getMessage()
    {
        return $this->Message;
    }
    public function SetMessage($Message)
    {
        $this->Message = $Message;
    }
    public function writeMessage()
    {
        $query ="INSERT INTO `chat_room_message`(`id_message`, `id_message_transmiter`, `id_room`, `ip`, `Token`, `Message`)
        VALUES (NULL,
        '".$this->id_message_transmiter."',
        '".$this->id_room."',
        '".$this->ip."',
        '".$this->Token."',
        '".$this->Message."')";

        $writemessage=$this->db()->query($query);
       if($writemessage){
           $status ="";
       }
       return $writemessage;
    }
    public function getAllMessageByUser($userip,$token)
    {
        $query = $this->db()->query("SELECT * FROM chat_room_message INNER JOIN chat_room ON chat_room_message.ip = '$userip' AND chat_room_message.Token = '$token'");
        if($query->num_rows > 0){
           
            while ($row = $query->fetch_object()) {
               $resultSet[]=$row;
            }
         
         return $resultSet;
        }
    }
    

    public function createRoom()
    {
        $roomquery ="SELECT * FROM `chat_room` WHERE ip_transmiter = '".$this->ip_transmiter."' AND Token = '".$this->Token."'";
        $result = $this->db()->query($roomquery);
        if ($result->num_rows == 0) {
            $query="INSERT INTO `chat_room` (`id_room`,`id_recept`,`id_transmiter`,`ip_transmiter`,`Token`)
        VALUES(NULL,
            '".$this->id_recept."',
            '".$this->id_transmiter."',
            '".$this->ip_transmiter."',
            '".$this->Token."'
        )";

        $createRoom=$this->db()->query($query);
        if($createRoom == true){
            $status ="Room Was Successfully Created.";
        }
        return $createRoom;
        }

        
    }

    public function roomexist()
    {
        $tok = $_COOKIE['Token'];
        $query ="SELECT `ip_transmiter`,`id_room` FROM `chat_room` WHERE `ip_transmiter` = '$tok'";
        
        $result = $this->db()->query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                //return $positive = "1";
            }
            
        } else {
            header("location: Index");
        }
    }

}

?>