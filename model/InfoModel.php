<?php
class InfoModel extends EntidadBase{
    private $company_address;
    private $company_city;
    private $company_phone;
    private $company_start_service;
    private $company_end_service;
    private $company_email_contact;
    public function __construct($adapter) {
        $table ="tb_info_location";
        parent:: __construct($table, $adapter);
    }
    public function get_company_address()
    {
        return $this->company_address;
    }
    public function set_company_address($company_address)
    {
        $this->company_address = $company_address;
    }
    public function get_company_city()
    {
        return $this->company_city;
    }
    public function set_company_city($company_city)
    {
        $this->company_city = $company_city;
    }
    public function get_company_phone()
    {
        return $this->company_phone;
    }
    public function set_company_phone($company_phone)
    {
        $this->company_phone = $company_phone;
    }
    public function get_company_start_service()
    {
        return $this->company_start_service;
    }
    public function set_company_start_service($company_start_service)
    {
        $this->company_start_service = $company_start_service;
    }
    public function get_company_end_service()
    {
        return $this->company_end_service;
    }
    public function set_company_end_service($company_end_service)
    {
        $this->company_end_service = $company_end_service;
    }
    public function get_company_email_contact()
    {
        return $this->company_email_contact;
    }
    public function set_company_email_contact($company_email_contact)
    {
        $this->company_email_contact = $company_email_contact;
    }
    
    public function company_info()
    {
        $query=$this->db()->query("SELECT * FROM tb_info_location");
        
        if($query ->num_rows > 0){
        while ($row = $query->fetch_object()) {
            
           $resultSet[]=$row;
            }

        }
    }
}
?>