<?php
class PayUAuth extends EntidadBase{
    private $idPu;
    private $apikey;
    private $accountId;
    private $merchantId;
    private $currency;
    private $test;
    private $responseUrl;
    private $confirmationUrl;

    public function __construct($adapter) {
        $table ="tb_payuauth";
        parent:: __construct($table,$adapter);
    }

    public function getIdPu()
    {
        return $this->idPu;
    } 
    public function setIdPu($idPu)
    {
        $this->idPu = $idPu;
    } 
    public function getApikey()
    {
        return $this->apikey;
    } 
    public function setApikey($apikey)
    {
        $this->apikey = $apikey;
    } 
    public function getAccountId()
    {
        return $this->accountId;
    } 
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;
    } 
    public function getMerchantId()
    {
        return $this->merchantId;
    } 
    public function setMerchantId($merchantId)
    {
        $this->merchantId = $merchantId;
    } 
    public function getCurrency()
    {
        return $this->currency;
    } 
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    } 
    public function getTest()
    {
        return $this->test;
    } 
    public function setTest($test)
    {
        $this->test = $test;
    } 
    public function getResponseUrl()
    {
        return $this->responseUrl;
    } 
    public function setResponseUrl($responseUrl)
    {
        $this->responseUrl = $responseUrl;
    } 
    public function getConfirmationUrl()
    {
        return $this->confirmationUrl;
    } 
    public function setConfirmationUrl($confirmationUrl)
    {
        $this->confirmationUrl = $confirmationUrl;
    }

    public function getPayUAuth()
    {
        $query =$this->db()->query("SELECT * FROM tb_payuauth WHERE Activation = 1");

        if($query->num_rows > 0){
            while ($row = $query->fetch_object()) {
                $resultSet[]=$row;
                }
            }
            else{}
            return $resultSet;
    }
}
    ?>