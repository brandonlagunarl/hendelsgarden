<?php
class Municipios extends EntidadBase{

    private $id_municipio;
    private $municipio;
    private $estado;
    private $departamento_id;

    public function __construct($adapter) {
        $table ="municipios";
        parent:: __construct($table, $adapter);
    }
    public function getId_municipio()
    {
        return $this->id_municipio;
    }
    public function setId_municipio($id_municipio)
    {
        $this->id_municipio = $id_municipio;
    }
    public function getMunicipio()
    {
        return $this->municipio;
    }
    public function setMunicipio($municipio)
    {
        $this->municipio = $municipio;
    }
    public function getEstado()
    {
        return $this->estado;
    }
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }
    public function getDepartamento_id()
    {
        return $this->departamento_id;
    }
    public function setDepartamento_id($departamento_id)
    {
        $this->departamento_id = $departamento_id;
    }

    public function getDepartamento()
    {
        
    }
}