<?php
class Category extends EntidadBase{

    private $id;
    private $FamilyTitle;
    private $FamilyDesc;
    private $Activation;

    public function __construct($adapter) {
        $table ="tb_FamilyProduct";
        parent:: __construct($table, $adapter);
    }

    public function getId()
    {
        return $this->id;
    }
    public function setId($id)
    {
        $this->id = $id;
    }
    public function getFamilyTitle()
    {
        return $this->FamilyTitle;
    }
    public function setFamilyTitle($FamilyTitle)
    {
        $this->FamilyTitle = $FamilyTitle;

    }
    public function getFamilyDesc()
    {
        return $this->FamilyDesc;
    }
    public function setFamilyDesc($FamilyDesc)
    {
        $this->FamilyDesc = $FamilyDesc;

    }
    public function getActivation()
    {
        return $this->Activation;
    } 
    public function setActivation($Activation)
    {
        $this->Activation = $Activation;

    }
    public function getCategory()
    {
        $query=$this->db()->query("SELECT * FROM tb_FamilyProduct INNER JOIN tb_Config_FamilyProducts ON tb_FamilyProduct.id = tb_Config_FamilyProducts.idFamily");
        
    if($query ->num_rows > 0){
           
        while ($row = $query->fetch_object()) {
           $resultSet[]=$row;
        }
    }
    else{
        echo "0 Rows";
    }
        return $resultSet;
    }

}