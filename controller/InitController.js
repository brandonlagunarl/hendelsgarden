$(document).ready(function() {
  valuecart();
  $(".dropdown-trigger").dropdown();
});

function loadAgents() {
  var object = JSON.parse(cities);

  console.log(object);
  var txt = "";
  for (i in object) {
    txt += "<p>" + object[i].municipio + "</p>";
  }
  txt += "";
  $("#cities").html(txt);
}

window.onload = function() {
  try {
    $("#city")
      .on("keyup", function() {
        var city = $(this).val();
        var value = $(this).val().length;
        $.post(
          "index.php?controller=Checkout&action=delivery",
          {
            city: city
          },
          function(response, status) {
            $("#rescity").html(response);
            //console.log(status);
          }
        );
      })
      .keyup();
  } catch (e) {}
};

function valuecart() {
  var i = "ok";
  $.ajax({
    url: "index.php?controller=Cart&action=valuecart",
    datatype: "json",
    async: true,
    cache: false,
    type: "POST",
    data: { i: i }
  }).done(function(response) {
    var parseobject = JSON.parse(response);
    var txt = "";
    for (i in parseobject) {
      txt += parseobject[i];
    }
    txt += "";
    $("#cart").html(txt);
  });
}
