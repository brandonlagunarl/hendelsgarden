<?php
class PageController extends ControladorBase{
    private $adapter;
    private $conectar;

    public function __construct() {
       parent::__construct();
        $this->conectar=new Conectar();  
        $this->adapter=$this->conectar->conexion();
    }
    public function Index()
    {
        $products = new Product($this->adapter);
        $productpage= $products->getProductAll();


       $this->view("pages/index",array(
           "tag" => "Paginas",
           "productpage" => $productpage
       ));
    }
}

?>