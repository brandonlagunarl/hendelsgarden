<?php
class QuestionController extends ControladorBase{

    private $adapter;
    private $conectar;

    public function __construct() {
       parent::__construct();

       $this->conectar=new Conectar();
        $this->adapter=$this->conectar->conexion();
    }

    public function Index() {
        $this->view("question/index",array(
            "tag" => "question"
        ));
        if(!empty($_GET)){
            if(isset($_GET['i'])){
            $i= $_GET['i'];
            if($i == 'trabaja_con_nosotros'){
                $this->redirect("Question", "trabaja_con_nosotros");
            }
            else{}
            }}else{

            }
            
        }

    public function trabaja_con_nosotros()
    {
        $agents = new JBlobal($this->adapter);
        $agent = $agents->getAgents();
        if(!empty($_POST['catch'])){
            $form=new Form($this->adapter);
        //setting method POST
        $form->setActivate('1');
        $form->setPostponed('0');
        $form->setNameClient($_POST['first_name']);
        $form->setLastNameClient($_POST['last_name']);
        $form->setAddressClient($_POST['address']);
        $form->setDateEntered(date("Y-m-d H:i:s"));
        $form->setEmailClient($_POST['email']);
        $form->setPhoneClient($_POST['phone']);
        $form->setEnableMsj('1');
        $form->setCity($_POST['city']);
        $form->setTypeDate('workwithus');
        //calling crud function and set all data
        $savedate=$form->savedate();
        $this->redirect('Index','');
        }else{

        $this->view("workwithus",array(
            // "allcities" => $allcities,
             "tag"   =>"Trabaja Con Nosotros"
         ));
        }
    }

}

?>