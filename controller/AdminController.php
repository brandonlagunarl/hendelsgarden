<?php
class AdminController extends ControladorBase{
        private $adapter;
        private $conectar;
    public function __construct() {
    parent::__construct();
    $this->conectar=new Conectar();  
    $this->adapter=$this->conectar->conexion();
    
    }

    public function Index()
    {
        $agents = new JBlobal($this->adapter);
        $agent = $agents->getAgents();
    if(isset($_SESSION['User'])){
        $this->adminview("Admin/main-structure",array(
            "tag" => "admin"
        ));
    }else{
        $this->redirect("Session","");
    }
    }

    public function search()
    {
        if(isset($_POST) && !empty($_POST)){
            $launch = $_POST['launch'];

            $this->frameview("Admin/search", array(
                "launch" => $launch,
            ));
        }
    }

    public function logout()
    {
        if(isset($_POST) && !empty($_POST)){
            session_start();
            $_SESSION = array();
                if (ini_get("session.use_cookies")) {
                $params = session_get_cookie_params();
                    setcookie(session_name(), '', time() - 42000,
                    $params["path"], $params["domain"],
                    $params["secure"], $params["httponly"]
                            );
        }
        session_destroy();
        $this->redirect("Index","Index");
        }
        
    }
    
    public function modules()
    {
        $this->frameview("Admin/modules", array());
    }



}

?>