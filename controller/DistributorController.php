<?php
class DistributorController extends Controladorbase{

    private $adapter;
    private $conectar;

    public function __construct() {
       parent::__construct();

       $this->conectar=new Conectar();
        $this->adapter=$this->conectar->conexion();
    }

    public function index()
    {
        $agents = new JBlobal($this->adapter);
        $agent = $agents->AllAgents();

        $this->view("caption/distributor",array("agents" => $agent));

    }

    public function footerDistributor()
    {
        if(isset($_POST['i']) && !empty($_POST['i'])){
           if($_POST['i'] == "ok"){
            $agents = new JBlobal($this->adapter);
            $agent = $agents->getAgents();
            $this->frameview("jsrequest/data", array(
                "data" => $agent,
            ));
           }
           
        }
    }

}

?>
