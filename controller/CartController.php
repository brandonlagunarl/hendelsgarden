<?php

class CartController extends ControladorBase{
    public $conectar;
	public $adapter;
	
    public function __construct() {
        parent::__construct();
		 
        $this->conectar=new Conectar();
        $this->adapter=$this->conectar->conexion();
        
    }

    public function Index()
    {
        if(isset($_COOKIE['Token']) && isset($_COOKIE['NoTouch']))
        {
            $cart = new Cart($this->adapter);
            $cart->setIpUser($_COOKIE['NoTouch']);
            $cart->setToken($_COOKIE['Token']);
            $cart->setEmailSent('0');
            $getcart=$cart->AuthCart($_COOKIE['NoTouch'],$_COOKIE['Token']);

           $this->redirect("Cart","Items");

        }else{ getAuth();
        $this->return("Cart","");
        }
        

    }
    public function addtocart()
    {
        
        if(isset($_POST['id']) && !empty($_POST['id'])){
                if(isset($_COOKIE['Token']) && isset($_COOKIE['NoTouch'])){
                        $idProduct = $_POST['id'];
                        $product = new Cart($this->adapter);
                        $product->setidProduct($idProduct);
                        $product->setidCart('2');
                        $product->setToken($_COOKIE['Token']);
                        $addtocart = $product->addtocart($_COOKIE['Token'],$idProduct);
                       
                        $this->frameview("jsrequest/status",array("status" => $addtocart));
                }else{ 
                    genAuth();
                }
        }
        else{
            $this->redirect("Index","");
        }
    }

    public function Items()
    {   
        $cart = new Cart($this->adapter);
        $getcart = $cart->getCart($_COOKIE['NoTouch'],$_COOKIE['Token']);
            foreach ($getcart as $cart ){}
                setcookie("idCart", $cart->idCart);
        $items = new Product($this->adapter);
        $getall=$items->GetItemsByCart($cart->idCart,$_COOKIE['Token']);
        $this->View("cart/index",array(
            "getcart" => $getcart,
            "getall" => $getall,
            "tag" => "Carro de compras"
        ));

    }

    public function cartvalue()
    {
        if(isset($_COOKIE['Token']) && isset($_COOKIE['NoTouch'])){
            $cart = new Cart($this->adapter);
            $getcart = $cart->jsGetCartValue($_COOKIE['Token']);
            foreach ($getcart as $cart);
            $row = array(
                "row" => $cart,
            );
            return json_encode($row);
            //echo json_encode($row);
        }
        
    }

    public function deleteitem()
    {
        if(isset($_POST['id'])){
            $id =$_POST['id'];
            $cart = new Cart($this->adapter);
            $delete = $cart->deleteitem($id);
            $this->frameview("jsrequest/status", array(
                "status" => $delete
            ));
        }
    }

    public function updatevalue()
    {
        if(isset($_POST['func']) && isset($_POST['id'])){
            $id =$_POST['id'];
            $func =$_POST['func'];

            $cart = new Cart($this->adapter);
            $updatevalue = $cart->updatevalue($func,$id,$_COOKIE['Token']);

                $this->frameview("jsrequest/status",array(
                    "status" => $updatevalue
                ));
        }
    }

    public function valuecart()
    {
        if(isset($_POST['i'])){

                $cart = new Cart($this->adapter);
                $getvalue = $cart->jsGetCartValue($_COOKIE['Token']);

            $this->frameview("jsrequest/valuecart",array(
                "data" => $getvalue
            ));
        }
    }


}
?>