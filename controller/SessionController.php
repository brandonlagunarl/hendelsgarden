<?php
class SessionController extends ControladorBase{
        private $adapter;
        private $conectar;
    public function __construct() {
    parent::__construct();
    $this->conectar=new Conectar();  
    $this->adapter=$this->conectar->conexion();
    }

    public function Index()
    {
        if(isset($_SESSION['User'])){
            $this->redirect("Admin","");
        }else{
        $this->view("login/index",array(
            "tag" => "login"
        ));
    }
    }
    public function NewSession()
    {
        $usr = $_POST['usr'];
        $pss = $_POST['pss'];
       if($usr != '' && $pss != ''){
            $session = new Session($this->adapter);
            $session->setusr($usr);
            $session->setpss($pss);
                $login=$session->Login();
                $this->redirect("Admin","");

       }else{
           $this->redirect("Session","");
       }
       
    }
    public function logout()
    {
       session_start();
       session_destroy();
       $this->redirect("Index","");
    }


}

?>