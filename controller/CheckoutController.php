<?php

class CheckoutController extends ControladorBase{
    public $conectar;
	public $adapter;
	
    public function __construct() {
        parent::__construct();
		 
        $this->conectar=new Conectar();
        $this->adapter=$this->conectar->conexion();
    }

    public function Index()
    {
            //if check was created
    if(isset($_COOKIE['Check'])){
        if($_COOKIE['Check'] == 1){
            $check = new Check($this->adapter);
            $validation = $check->getCheck($_COOKIE['NoTouch'],$_COOKIE['Token']);
                foreach($validation as $check){}
                    $payment = $check->Payment;
                    switch ($payment) {
                    case '1':

                                //sending base email 
                                $cart = new Cart($this->adapter);
                                $getcart = $cart->getCart($_COOKIE['NoTouch'],$_COOKIE['Token']);
                                if($getcart != ""){
                                    foreach ($getcart as $cart ){}
                                    $items = new Product($this->adapter);
                                    $getall=$items->GetItemsByCart($cart->idCart,$_COOKIE['Token']);
                                    
                                    $this->email("checkout/contraentrega",array(
                                    "cart" => $getcart,
                                    "check" => $validation,
                                    "product" => $getall,
                                    ));
                                    $check = new Check($this->adapter);
                                    $getcheck=$check->getCheck($_COOKIE['NoTouch'],$_COOKIE['Token']);
                                    foreach ($getcheck as $checked) {}
                                        
                                    $check = new Check($this->adapter);
                                    $check->setToken($_COOKIE['Token']);
                                    $check->setActived('1');
                                    $check->settransactionState('1');
                                    $savedata = $check->savestatus($checked->id_check);

                                /*functions to reset tokenization and validate sales*/
                                setcookie("CheckedExist",$_COOKIE['Token']);
                                setcookie("Token","0",time()-1);
                                setcookie("Check","0",time()-1);
                                $this->redirect("index","");
                                
                                }

                       break;

                        case '2':
                        echo "pago por efecty ";
                         break;
                         case '3':
                        echo "pago por PayU Latam <br>";
                                    //check actually getted
                                    //get cart
                                    $cart = new Cart($this->adapter);
                                    $getcart = $cart->getCart($_COOKIE['NoTouch'],$_COOKIE['Token']);
                                    if($getcart != ""){
                                            foreach ($getcart as $cart ){}
                                    //get items by cart
                                    $items = new Product($this->adapter);
                                    $getall=$items->GetItemsByCart($cart->idCart,$_COOKIE['Token']);
                                    //get payu credentials
                                    $payu = new PayUAuth($this->adapter);
                                    $credential = $payu->getPayUAuth();
                                            foreach($credential as $credentials){}
                                    //manipulando datos de productos
                                    $total = 0;
                                    foreach ($getall as $products){
                                    $subtotal = ($products->PriceProduct * $products->Quantity);
                                    $total += number_format($products->PriceProduct * $products->Quantity, 3, '.', ',');  
                                }
                                    $htotal= number_format($total, 3, '', '');
                                    //signature value
                                    $signature = md5($credentials->apikey."~".$credentials->merchantId."~Ref00".$cart->idCart."~".$htotal."~".$credentials->currency);
                                    //$signature = md5("4Vj8eK4rloUd272L48hsrarnUA~508029~Ref0097~422.000~COP");
                                    $this->frameview("checkout/payumethod",array(
                                        "check" => $validation,
                                        "getcart" => $getcart,
                                        "getproducts" => $getall,
                                        "getcredentials" => $credential,
                                        "htotal" => $htotal,
                                        "signature" => $signature,
                                    ));
                            }
                         break;

                    default:
                        $this->redirect("index","");
                        break;
                }
            /*$check = new Check($this->adapter);
            $validation = $check->getCheck($_COOKIE['NoTouch'],$_COOKIE['Token']);

            //productos
            $cart = new Cart($this->adapter);
                $getcart = $cart->getCart($_COOKIE['NoTouch'],$_COOKIE['Token']);
                    foreach ($getcart as $cart ){}
                        setcookie("idCart", $cart->idCart);
                    $items = new Product($this->adapter);
                    $getall=$items->GetItemsByCart($cart->idCart,$_COOKIE['Token']);

        $this->view("checkout/invoice",array(
            "getcart"       =>      $getcart,
            "getall"        =>      $getall,
            "validation"    =>      $validation,
            "tag"           =>      " Factura"
        ));*/
        }
        else{
            $this->redirect("Checkout","");
        }
    }else{
        
        $cart = new Cart($this->adapter);
        $getcart = $cart->getCart($_COOKIE['NoTouch'],$_COOKIE['Token']);
        if($getcart != ""){
            foreach ($getcart as $cart ){}
                setcookie("idCart", $cart->idCart);
        $items = new Product($this->adapter);
        $getall=$items->GetItemsByCart($cart->idCart,$_COOKIE['Token']);

                $payment = new Cart($this->adapter);
                $paymentmethod = $payment->getPayment();

        $this->view("checkout/index",array(

            "getcart" => $getcart,
            "getall" => $getall,
            "paymentmethod" => $paymentmethod, 
            "tag" => "Finalizar pedido"
        ));
    }
        //post form 
        if(!empty($_POST)){
            $check = new Check($this->adapter);
            $check->setBuyerName($_POST['name']);
            $check->setBuyerLastName($_POST['lastname']);
            $check->setDocument($_POST['document']);
            $check->setBuyerCity($_POST['city']);
            $check->setBuyerEmail($_POST['email']);
            //$check->setBuyerAge($_POST['age']);
            $check->setBuyerPhone($_POST['phone']);
            $check->setBuyerDelivery($_POST['delivery']);
            $check->setBuyerAnexedDelivery($_POST['anexed']);
            $check->setPayment($_POST['payment']);
            $check->setDeliveryTime($_POST['datepicker']);
            $check->setToken($_COOKIE['Token']);
            $check->setIp($_COOKIE['NoTouch']);
            $check->setActived('0');
            $savedata = $check->savedata();
        }
    }
    }

    public function validation()
    {
        if(isset($_GET['i'])){
            if($_GET['i'] == 1){
                setcookie('Check','1');
                $this->redirect("Checkout","");
            }else{
                setcookie('Check','0');
                $this->redirect("Checkout","");
            }
        }
    }

    public function response()
    {
        $check = new Check($this->adapter);
            $getcheck=$check->getCheck($_COOKIE['NoTouch'],$_COOKIE['Token']);
            foreach ($getcheck as $checked) {}

        $check = new Check($this->adapter);
        //block check by payed status
        $check->setToken($_COOKIE['Token']);
        $check->setActived('1');
        $check->settransactionState($_REQUEST['transactionState']);
        $savedata = $check->savestatus($checked->id_check);

        /*functions to reset tokenization and validate sales*/
        setcookie("CheckedExist",$_COOKIE['Token']);
        setcookie("Token","0",time()-1);
        setcookie("Check","0",time()-1);
        
        $this->view("checkout/responsepayu",array(
        ));
    }

    public function confirmation()
    {
        $this->view("chekout/confirmationpayu",array(
        ));
    }

    public function delivery()
    {
    if(isset($_POST['city'])){
        $dirty = $_POST['city'];
        $city = cln_str($dirty);
        $delivery =new JBlobal($this->adapter);
        $getrate= $delivery->getRates($city);
        
        $this->frameview("jsrequest/delivery",array(
            "getrate"   => $getrate, 
            "city"      => $city
        ));

    }
else{//$this->redirect("Index","");
}   
}
}