<?php

class ProductsController extends ControladorBase{
    public $conectar;
	public $adapter;
	
    public function __construct() {
        parent::__construct();
		 
        $this->conectar=new Conectar();
        $this->adapter=$this->conectar->conexion();
    }

    public function Index()
    {
        $category = new Category($this->adapter);
        $allcategory = $category->getCategory();

        $products = new Product($this->adapter);
        $allproducts =$products->getAll();
        
       $this->view("product/index",array(
            "allcategory" => $allcategory,
            "allproducts" => $allproducts,
            "tag" => "Nuestos Productos",
            "desc" => "Categorias",
       ));
    }
    public function Detail(){
        if(isset($_GET)){
            if(isset($_GET['i']) && !empty($_GET['i'])){
                $i = $_GET['i'];

		        //Producto
                    $producto=new Product($this->adapter);
		            $allproducts=$producto->getProductByName($i);
       
                    if($allproducts == NULL){
                        $this->redirect('','');
                    }
                //Cargamos la vista index y le pasamos valores
                    $this->view("product/Description",array(
			        "allproducts" => $allproducts,
                    "tag"    =>"$i"
                    ));
            }

        }
        
    }

    public function CategoryGroup(){
        if(isset($_GET['filter']) && !empty($_GET['filter'])){
            $category = new Category($this->adapter);
            $allcategory =$category->getBy('id',$_GET['filter']);

        foreach ($allcategory as $category){
            $product = new Product($this->adapter);
            $allproducts =$product->getProductsbyCategory($category->id);
            
                foreach($allproducts as $products){
                }
                }
                $this->view("product/index",array(
                    "allcategory" => $allcategory,
                    "allproducts" => $allproducts,
                    "tag" => "Nuestos Productos",
                    "desc" => "Productos",
               ));
        }else{
            echo "no filtrado";
        }
        

        }
        

    public function Desc()
    {
        if(isset($_GET)){
            if(isset($_GET['i']) && !empty($_GET['i'])){
                $i = $_GET['i'];

		        //Producto
                    $producto=new Product($this->adapter);
		            $allproducts=$producto->getProductByName($i);
       
                    if($allproducts == NULL){
                        $this->redirect('','');
                    }
                //Cargamos la vista index y le pasamos valores
                    $this->view("product/desc",array(
			        "allproducts" => $allproducts,
                    "tag"    =>"$i"
                    ));
            }

        }

    }
    public function PrductList()
    {
        $productlist = new Product($this->adapter);
        $allproducts = $productlist->getAll();

        if($allproducts == NULL){
            echo "0 productos en la lista";
        }else{
            $this->view("list/productList",array(
                "allproducts" => $allproducts,
                "tag"    =>"Hendelsgarden"
                ));
        }
    }

    public function padd()
    {
        if(!empty($_POST)){
            $padd= new Product($this->adapter);
            $padd->setNameProduct($_POST['NameProduct']);
            $padd->setPriceProduct($_POST['PriceProduct']);
            $padd->setDescProduct($_POST['DescProduct']);
            $padd->setReLink($_POST['ReLink']);
            $padd->setidFamily("1");
            $padd->setActiveProduct("1");
            $padd->setActiveDiscount("1");
                    //convert any mayus to minus
                    $lower = strtolower($_POST['NameProduct']);
                    //replace espace to '_' product name
                    $padd->setMetaKeyWord(str_replace(" ", "_", $lower));
            $saveproduct = $padd->saveproduct();
            echo $_POST['NameProduct'];
            echo $lower;
        }else{
            $this->redirect("Index","");
        }
    }

    public function seemore()
    {
    }

}

    ?>
