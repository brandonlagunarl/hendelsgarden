<?php require_once 'config/global.php';
        //require_once 'config/Api.php';
?>
<head>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Anton|Merienda|Quicksand|Courgette|Kalam|Bad+Script" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<?php
$initial = INITIAL;
foreach(glob("dependences/materializeV1.0.0/*.css") as $style){
    echo "<link rel='stylesheet' type='text/css' href='/".INITIAL."/hendelsgarden/".$style."'> ";
}

foreach(glob("dependences/animatecssV1.0.0/*.css") as $style){
    echo "<link rel='stylesheet' type='text/css' href='/".INITIAL."/hendelsgarden/".$style."'> ";
}
foreach(glob("dependences/animatecssV1.0.0/*.js") as $animate){
    echo "<script src='/".INITIAL."/hendelsgarden/".$animate."'> </script>";
}
foreach(glob("dependences/mainappV0.0.1/*.css") as $css){
    echo "<link rel='stylesheet' type='text/css' href='/".INITIAL."/hendelsgarden/".$css."'> ";
}
foreach(glob("dependences/materializeV1.0.0/*.js") as $script){
    echo "<script src='/".INITIAL."/hendelsgarden/".$script."'> </script>";
}
foreach(glob("dependences/individual/js/*.js") as $script){
    echo "<script src='/".INITIAL."/hendelsgarden/".$script."'> </script>";
}
foreach(glob("controller/*.js") as $script){
    echo "<script src='/".INITIAL."/hendelsgarden/".$script."'> </script>";
}
foreach(glob("dependences/parollerV1.2.9/*.js") as $parollex){
    echo "<script src='/".INITIAL."/hendelsgarden/".$parollex."'></script>";
}
foreach(glob("dependences/scripts/*.js") as $script){
    echo "<script src='/".INITIAL."/hendelsgarden/".$script."'></script>";
}
?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXShhfsOn6C0Igw3sfcipgFRROKRK7-dc&callback=initMap"
          async defer></script>
</head>